package com.egci428.a10660;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private CookieJar dataSource;
    private ListView cookieList = null;
    private CookieAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        updateItem();

        cookieList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
                final Cookie cookie = (Cookie) cookieList.getAdapter().getItem(i);

                view.animate().setDuration(666).alpha(0).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        dataSource.deleteCookie(cookie);
                        updateItem();
                        view.setAlpha(1);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addBtn:
                //go to add cookie
                Intent intent = new Intent(this, NewFortuneActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateItem() {
        cookieList = (ListView) findViewById(R.id.list_view);

        dataSource = new CookieJar(this);
        dataSource.open();

        final List<Cookie> cookies = dataSource.getAllCookies();
        adapter = new CookieAdapter(this, cookies);
        cookieList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        updateItem();
        super.onResume();
    }

    @Override
    public void onPause() {
        dataSource.close();
        super.onPause();
    }
}
// https://guides.codepath.com/android/Using-the-App-ToolBar#using-toolbar-as-actionbar
// https://www.raywenderlich.com/124438/android-listview-tutorial