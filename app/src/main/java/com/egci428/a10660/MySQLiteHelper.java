package com.egci428.a10660;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by VerifyFX on 12/11/2016.
 */

public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cookieJar.db";
    public static final String TABLE_COOKIES = "cookies";
    private static final int DATABASE_VERSION = 1;

    public static final String COLUMN_ID     = "_id";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    //Database creation
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_COOKIES + "( " +
                                                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                    COLUMN_COMMENT + " TEXT NOT NULL, " +
                                                    COLUMN_TIMESTAMP + " INTEGER NOT NULL" +
                                                    ");";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COOKIES);
        onCreate(db);
    }
}
