package com.egci428.a10660;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VerifyFX on 12/11/2016.
 */

public class CookieJar {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_COMMENT,
            MySQLiteHelper.COLUMN_TIMESTAMP
    };

    public CookieJar(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Cookie createCookie(String comment, long timeStamp) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_COMMENT, comment);
        values.put(MySQLiteHelper.COLUMN_TIMESTAMP, timeStamp);
        long insertId = database.insert(MySQLiteHelper.TABLE_COOKIES, null, values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_COOKIES, allColumns, MySQLiteHelper.COLUMN_ID+" = "+insertId, null, null, null, null);
        cursor.moveToFirst();
        Cookie newCookie = cursorToCookie(cursor);
        cursor.close();
        return newCookie;
    }

    public void deleteCookie(Cookie cookie) {
        long id = cookie.getId();
        database.delete(MySQLiteHelper.TABLE_COOKIES, MySQLiteHelper.COLUMN_ID+" = "+id, null);
    }

    public List<Cookie> getAllCookies() {
        List<Cookie> cookies = new ArrayList<>();
        Cursor cursor = database.query(MySQLiteHelper.TABLE_COOKIES, allColumns,  null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Cookie cookie = cursorToCookie(cursor);
            cookies.add(cookie);
            cursor.moveToNext();
        }
        return cookies;
    }

    private Cookie cursorToCookie(Cursor cursor) {
        Cookie cookie = new Cookie();
        cookie.setId(cursor.getLong(0));
        cookie.setComment(cursor.getString(1));
        cookie.setTimestamp(cursor.getLong(2));
        return cookie;
    }
}
