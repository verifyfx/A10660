package com.egci428.a10660;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.List;

/**
 * Created by VerifyFX on 12/11/2016.
 */

public class CookieAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Cookie> mDataSource;

    public CookieAdapter(Context context, List<Cookie> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public  int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int pos) {
        return mDataSource.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.list_row, parent, false);
        TextView comment = (TextView) rowView.findViewById(R.id.lblCookie);
        TextView time    = (TextView) rowView.findViewById(R.id.lblTime);
        ImageView image  = (ImageView) rowView.findViewById(R.id.list_image);

        Cookie cookie = (Cookie) getItem(pos);
        comment.setText(cookie.getComment());
        time.setText("Date: "+DateFormat.getDateTimeInstance().format(cookie.getTimestamp()));
        switch (cookie.getComment()) {
            case "You will get A":
                image.setImageResource(R.drawable.opened_cookie_gradea);
                break;
            case "You're lucky":
                image.setImageResource(R.drawable.opened_cookie_lucky);
                break;
            case "Don't panic":
                image.setImageResource(R.drawable.opened_cookie_panic);
                comment.setTextColor(Color.parseColor("#FF6600"));
                break;
            case "Something surprise you today":
                image.setImageResource(R.drawable.opened_cookie_surprise);
                break;
            case "Work harder":
                image.setImageResource(R.drawable.opened_cookie_work);
                comment.setTextColor(Color.parseColor("#FF6600"));
                break;
            default:
                break;
        }

        return rowView;
    }
}
