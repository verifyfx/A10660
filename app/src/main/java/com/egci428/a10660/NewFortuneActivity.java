package com.egci428.a10660;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

public class NewFortuneActivity extends AppCompatActivity {

    private CookieJar dataSource;

    Toolbar toolbar;
    TextView shakeLbl;
    TextView timeLbl;
    TextView resultLbl;
    ImageView image;

    String commentStr = null;
    long timeStamp    = 0;

    private SensorManager mSensorManager;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    boolean isShaken = false;
    boolean goBack   = false;
    boolean isReadyToShake = false;
    int recentShake = 0;
    int shakeCount = 0;

    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter

            //action
            if(mAccel > 12 && isReadyToShake && !isShaken) {
                shakeCount++;
                shakeLbl.setText("Shaking...");
                recentShake = 40;
                if(shakeCount>20) isShaken = true;
            } else if(isReadyToShake && !isShaken && recentShake<=0){
                shakeLbl.setText("Shake");
            }
            if(!isShaken) {
                recentShake--;
                if(recentShake==Integer.MIN_VALUE) recentShake = 0;
            }
            if (isShaken && !goBack) {
                Random r = new Random();
                switch (r.nextInt(5)) {
                    case 0:
                        //gradeA
                        commentStr = "You will get A";
                        image.setImageResource(R.drawable.opened_cookie_gradea);
                        break;
                    case 1:
                        //lucky
                        commentStr = "You're lucky";
                        image.setImageResource(R.drawable.opened_cookie_lucky);
                        break;
                    case 2:
                        //panic
                        commentStr = "Don't panic";
                        resultLbl.setTextColor(Color.parseColor("#FF6600"));
                        image.setImageResource(R.drawable.opened_cookie_panic);
                        break;
                    case 3:
                        //surprise
                        commentStr = "Something surprise you today";
                        image.setImageResource(R.drawable.opened_cookie_surprise);
                        break;
                    case 4:
                        //work
                        commentStr = "Work harder";
                        resultLbl.setTextColor(Color.parseColor("#FF6600"));
                        image.setImageResource(R.drawable.opened_cookie_work);
                        break;
                    default:
                        break;
                }
                timeStamp = System.currentTimeMillis();
                resultLbl.setText("Result: " + commentStr);
                shakeLbl.setText("Save");
                timeLbl.setText("Date: "+DateFormat.getDateTimeInstance().format(new Date()));
                goBack = true;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_fortune);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        shakeLbl = (TextView) findViewById(R.id.stateLbl);
        timeLbl = (TextView) findViewById(R.id.timeLbl);
        resultLbl = (TextView) findViewById(R.id.resultLbl);
        image = (ImageView) findViewById(R.id.imageView);

        //db
        dataSource = new CookieJar(this);
        dataSource.open();

        //shake
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        //shake touch
        shakeLbl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    return true;
                }
                if(motionEvent.getAction() == motionEvent.ACTION_UP) {
                    isReadyToShake = true;
                    shakeLbl.setText("Shake");
                    if(isShaken && goBack) { //save cookie
                        dataSource.createCookie(commentStr,timeStamp);
                        finish();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        dataSource.open();
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        super.onResume();
    }

    @Override
    protected void onPause() {
        dataSource.close();
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }
}

// http://stackoverflow.com/questions/2317428/android-i-want-to-shake-it